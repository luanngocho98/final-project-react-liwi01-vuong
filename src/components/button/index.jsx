import { Button, useTheme } from '@mui/material'
import React from 'react'

export default function ButtonMore(props) {
  const theme = useTheme()
  const { contentButton, handleOnClick } = props

  return (
    <Button
      onClick={handleOnClick}
      sx={{
        color: theme.palette.primary.light,
        backgroundColor: theme.palette.primary.lighter,
        padding: '12px 30px',
        borderRadius: '25px',
        textTransform: 'capitalize',
        ':hover': {
          backgroundColor: theme.palette.primary.light,
          color: theme.palette.primary.lighter,
        },
      }}
    >
      {contentButton}
    </Button>
  )
}
