import lg1 from '../assets/images/game-01.jpg'
import lg2 from '../assets/images/game-02.jpg'
import lg3 from '../assets/images/game-03.jpg'
import fg1 from '../assets/images/featured-01.jpg'
import fg2 from '../assets/images/featured-02.jpg'
import fg3 from '../assets/images/featured-03.jpg'
import yl1 from '../assets/images/service-01.jpg'
import yl2 from '../assets/images/service-02.jpg'
import yl3 from '../assets/images/service-03.jpg'
import ls1 from '../assets/images/stream-01.jpg'
import ls2 from '../assets/images/stream-02.jpg'
import ls3 from '../assets/images/stream-03.jpg'
import ls4 from '../assets/images/stream-04.jpg'
import av1 from '../assets/images/avatar-01.jpg'
import av2 from '../assets/images/avatar-02.jpg'
import av3 from '../assets/images/avatar-03.jpg'
import av4 from '../assets/images/avatar-04.jpg'

const initalState = {
  rows: [],
  library: [
    {
      gameId: 1,
      gameName: 'Dota 2',
      gameType: 'Sandbox',
      dataAdded: '24/08/2036',
      hoursPlayed: '634 H 22 Mins',
      currently: 'Downloaded',
      image: lg1,
      downloaded: true,
      star: 4.9,
      downloads: '2.2M',
    },
    {
      gameId: 2,
      gameName: 'Fortnite',
      gameType: 'Sandbox',
      dataAdded: '22/06/2036',
      hoursPlayed: '740 H 52 Mins',
      currently: 'Download',
      image: lg2,
      downloaded: false,
      star: 4.9,
      downloads: '2.2M',
    },
    {
      gameId: 3,
      gameName: 'CS-GO',
      gameType: 'Legendary',
      dataAdded: '21/04/2036',
      hoursPlayed: '892 H 14 Mins',
      currently: 'Downloaded',
      image: lg3,
      downloaded: true,
      star: 4.9,
      downloads: '2.2M',
    },
  ],
  game: [
    {
      id: 1,
      gameName: 'CS-GO',
      downloads: '249K Downloads',
      star: 4.8,
      downloaded: '2.3M',
      image: fg1, 
    },
    {
      id: 2,
      gameName: 'Gamezer',
      downloads: '249K Downloads',
      star: 4.8,
      downloaded: '2.3M',
      image: fg2,
    },
    {
      id: 3,
      gameName: 'Island Rusty',
      downloads: '249K Downloads',
      star: 4.8,
      downloaded: '2.3M',
      image: fg3,
    },
    {
      id: 4,
      gameName: 'Gamezer',
      downloads: '249K Downloads',
      star: 4.8,
      downloaded: '2.3M',
      image: fg2,
    },
    {
      id: 5,
      gameName: 'Island Rusty',
      downloads: '249K Downloads',
      star: 4.8,
      downloaded: '2.3M',
      image: fg3,
    },
  ],
  yourlive: [
    {
      id: 1,
      title: 'Go To Your Profile',
      content: "Cyborg Gaming is free HTML CSS website template provided by TemplateMo. This is Bootstrap v5.2.0 layout.",
      image: yl1,
    },
    {
      id: 2,
      title: 'Live Stream Button',
      content: "If you wish to support us, you can make a small contribution via PayPal to info [at] templatemo.com",
      image: yl2,
    },
    {
      id: 3,
      title: 'You Are Live',
      content: "You are not allowed to redistribute this template's downloadable ZIP file on any other template collection website.",
      image: yl3,
    },
  ],
  livestreams: [
    {
      id: 1,
      streamName: 'KenganC',
      imageProfile: av1,
      title: 'Just Talking With Fans',
      streamBackground: ls1,
      view: '1.2K',
      game: 'CS-GO',
      status: 'Live',
    },
    {
      id: 2,
      streamName: 'LunaMa',
      imageProfile: av2,
      title: 'CS-GO 36 Hours Live Stream',
      streamBackground: ls2,
      view: '1.2K',
      game: 'CS-GO',
      status: 'Live',
    },
    {
      id: 3,
      streamName: 'Areluwa',
      imageProfile: av3,
      title: "Maybe Nathej Allnight Chillin'",
      streamBackground: ls3,
      view: '1.2K',
      game: 'CS-GO',
      status: 'Live',
    },
    {
      id: 4,
      streamName: 'GangTm',
      imageProfile: av4,
      title: 'Live Streaming Till Morning',
      streamBackground: ls4,
      view: '1.2K',
      game: 'CS-GO',
      status: 'Live',
    },
  ],
  fortnite: {
    gameName: 'Fortnite',
    gameType: 'Sandbox',
    star: 4.8,
    downloads: '2.3M',
    storage: '36GB',
    action: 'Action',
  },
  
}

const mostpopularrightnowReducer = (state = initalState, action) => {
  switch (action.type){
    case 'SET_MOSTPOPULAR': {
      return {
        ...state,
        rnows: action.payload.data,
      }
    }
    default:
      return state
  }
}
export default mostpopularrightnowReducer;