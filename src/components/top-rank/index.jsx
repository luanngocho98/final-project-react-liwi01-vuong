import { Box, useTheme } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import Title from '../title'
import DownloadIcon from '@mui/icons-material/Download';
import StarIcon from '@mui/icons-material/Star';

export default function TopRank() {
  const theme = useTheme()
  const { library } = useSelector(state => state.mostpopularrightnow)
  const FirstContent = styled('div')({
    fontSize: '15px',
    fontWeight: '700',
    color: theme.palette.primary.light,
  })
  const SecondContent = styled('div')({
    fontSize: '15px',
    color: theme.palette.primary.main,
    margin: '8px 0px'
  })

  return (
    <Box
      sx={{
        width: '27%',
        padding: '30px',
        backgroundColor: theme.palette.primary.darker,
        borderRadius: '23px',
        overflow: 'auto',
        maxHeight: '72.14611872146119vh',
        '::-webkit-scrollbar': {
          width: '1px'
        }
      }}
    >
      <Box
        sx={{
          marginBottom: '30px',
        }}
      >
        <Title
          titleLeft='Top'
          titleRight='Downloaded'
        />
      </Box>
      <Box>
        {
          library.map((item) => {
            return (
              <Box
                key={item.gameId}
                sx={{
                  borderBottom: `1px solid ${theme.palette.primary.dark}`,
                  marginBottom: '28px',
                  paddingBottom: '28px',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  '& img': {
                    borderRadius: '23px',
                  }
                }}
              >
                <img src={item.image} alt={item.gameName} />
                <Box>
                  <FirstContent>{item.gameName}</FirstContent>
                  <SecondContent>{item.gameType}</SecondContent>
                  <Box>
                    <Box
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                      }}
                    >
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          fontSize: '15px',
                          marginRight: '16px',
                          color: theme.palette.primary.light,
                          '& svg': {
                            width: '16px',
                            height: '16px',
                            color: theme.palette.primary.yellow,
                          }
                        }}
                      >
                        <StarIcon />
                        &nbsp;{item.star}
                      </Box>
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          fontSize: '15px',
                          color: theme.palette.primary.light,
                          '& svg': {
                            width: '16px',
                            height: '16px',
                            color: theme.palette.primary.lighter,
                          }
                        }}
                      >
                        <DownloadIcon />
                        &nbsp;{item.downloads}
                      </Box>
                    </Box>

                  </Box>
                </Box>
                <Box>
                  <Box
                    sx={{
                      height: '24px',
                      borderRadius: '50%',
                      backgroundColor: theme.palette.primary.dark,
                      padding: '10px',
                      cursor: 'pointer',
                      marginTop: '33px',
                      ':hover': {
                        backgroundColor: theme.palette.primary.light,
                      },
                      '& svg': {
                        color: theme.palette.primary.lighter,
                      }
                    }}
                  >
                    <DownloadIcon />
                  </Box>
                </Box>
              </Box>
            )
          })
        }
      </Box>
      <Box
        sx={{
          textAlign: 'center',
          fontSize: '15px',
          color: theme.palette.primary.lighter,
          fontWeight: '600',
        }}
      >
        View All Games
      </Box>
    </Box>
  )
}
