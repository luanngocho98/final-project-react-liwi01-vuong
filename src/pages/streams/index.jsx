import { Box, Grid, Typography, useTheme } from '@mui/material'
import React from 'react'
import ButtonMore from '../../components/button'
import FeaturedGame from '../../components/featered-game'
import Title from '../../components/title'
import CheckIcon from '@mui/icons-material/Check'
import SportsEsportsIcon from '@mui/icons-material/SportsEsports'
import VisibilityIcon from '@mui/icons-material/Visibility'
import { useSelector } from 'react-redux'
import TopStreamer from '../../components/top-streamer'

export default function Streams() {
  const theme = useTheme()
  const { livestreams } = useSelector(state => state.mostpopularrightnow)

  return (
    <Box>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <FeaturedGame />
        <TopStreamer />
      </Box>
      <Box
        sx={{
          marginTop: '60px',
          borderRadius: '23px',
          backgroundColor: theme.palette.primary.darker,
          padding: '30px',
        }}
      >
        <Box
          sx={{
            marginButtom: '30px',
          }}
        >
          <Title
            titleLeft='Most Popular'
            titleRight='Live Stream'
          />
        </Box>
        <Box
          sx={{
            margin: '30px 0px'
          }}
        >
          <Grid container spacing={4}>
            {
              livestreams.map((item) => {
                return (
                  <Grid item sm={3} key={item.id}>
                    <Box
                      sx={{
                        '& .MuiTypography-root': {
                          display: 'none'
                        },
                        position: 'relative',
                        '& img': {
                          borderRadius: '23px',
                          width: '100%'
                        },
                        ':hover': {
                          '& .MuiTypography-root': {
                            display: 'unset',
                          },
                          '& .MuiBox-root': {
                            visibility: 'unset',
                          }
                        }
                      }}
                    >
                      <img src={item.streamBackground} alt={item.streamName} />
                      <Typography
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '5px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          right: '1.0980966325036603vw',
                          top: '15px',
                          cursor: 'pointer',
                        }}
                      >
                        {item.status}
                      </Typography>
                      <Box
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '3px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          bottom: '15px',
                          left: '15px',
                          cursor: 'pointer',
                          display: 'flex',
                          alignItems: 'center',
                          visibility: 'hidden',
                          '& svg': {
                            color: theme.palette.primary.light,
                          }
                        }}
                      >
                        <VisibilityIcon />
                        &nbsp;{item.view}
                      </Box>
                      <Box
                        sx={{
                          position: 'absolute',
                          backgroundColor: theme.palette.primary.lighter,
                          padding: '3px 10px',
                          borderRadius: '23px',
                          color: theme.palette.primary.light,
                          fontSize: '14px',
                          bottom: '15px',
                          right: '15px',
                          cursor: 'pointer',
                          display: 'flex',
                          alignItems: 'center',
                          visibility: 'hidden',
                          '& svg': {
                            color: theme.palette.primary.light,
                          }
                        }}
                      >
                        <SportsEsportsIcon />
                        &nbsp;{item.game}
                      </Box>
                    </Box>
                    <Box
                      sx={{
                        display: 'flex',
                        marginTop: '16px',
                      }}
                    >
                      <Box
                        sx={{
                          marginRight: '15px',
                          '& img': {
                            borderRadius: '50%',
                          }
                        }}
                      >
                        <img src={item.imageProfile} alt={item.streamName} />
                      </Box>
                      <Box>
                        <Box
                          sx={{
                            display: 'flex',
                          }}
                        >
                          <Box
                            sx={{
                              backgroundColor: theme.palette.primary.lighter,
                              borderRadius: '50%',
                              padding: '1px 4px',
                              marginRight: '3px',
                              '& svg': {
                                color: theme.palette.primary.light,
                                width: '12px',
                                height: '12px',
                              }
                            }}
                          >
                            <CheckIcon />
                          </Box>
                          <Typography
                            sx={{
                              fontSize: '14px',
                              color: theme.palette.primary.lighter,
                            }}
                          >
                            {item.streamName}
                          </Typography>
                        </Box>
                        <Typography
                          sx={{
                            marginTop: '8px',
                            fontSize: '20px',
                            fontWeight: '700',
                            color: theme.palette.primary.light,
                          }}
                        >
                          {item.title}
                        </Typography>
                      </Box>
                    </Box>
                  </Grid>
                )
              })
            }
          </Grid>
        </Box>
        <Box textAlign="center" mb="-50px">
          <ButtonMore contentButton='Load More Streams' />
        </Box>
      </Box>
    </Box>
  )
}
