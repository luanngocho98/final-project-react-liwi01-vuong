import { Box, Card, CardMedia, useTheme } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import StarIcon from '@mui/icons-material/Star';
import DownloadIcon from '@mui/icons-material/Download';
import styled from 'styled-components'
import Title from '../title';
import ButtonMore from '../button';
import { useEffect } from 'react';
import { getMostPopular } from '../../services/most-popular';
import { useState } from 'react';

export default function MostPopularRightNow() {
	const [ datacurrent, setDataCurrent ] = useState([])
	const [ isClicked,setIsClicked ] = useState(false)
	const theme = useTheme()
	const dispathch = useDispatch()
	const { rnows } = useSelector(state => state.mostpopularrightnow)
	const GameNameContent = styled('div')({
		color: theme.palette.primary.light,
		fontSize: '15px',
		fontWeight: '700',
	})
	const GameTypeContent = styled('div')({
		color: theme.palette.primary.main,
		fontSize: '15px',
		fontWeight: '400',
	})
	const GameAmountContent = styled('div')({
		color: theme.palette.primary.light,
		fontSize: '14px',
		fontWeight: '400',
	})
	const handleMore = () => {
		setDataCurrent(rnows)
		setIsClicked(!isClicked)
	}
	
	useEffect(() => {
		getMostPopular(
			(res) => {
				const { data } = res
				dispathch({
					type: 'SET_MOSTPOPULAR',
					payload: {
						data
					}
				})
				setDataCurrent(data?.filter((item,index) => index < 8))
			},
			(err) => {
				console.log(err)
			}
		)
	}, [dispathch])

	return (
		<Box
			sx={{
				marginBottom: '60px',
				padding: '30px',
				backgroundColor: theme.palette.primary.darker,
				borderRadius: '23px',
			}}
		>
			<Box
				sx={{
					marginBottom: '30px',
					padding: '0px 12px'
				}}
			>
				<Title
					titleLeft= 'Most Popular'
					titleRight= 'Right Now'
				/>
			</Box>
			<Box
				sx={{
					display: 'grid',
					gridTemplateColumns: 'auto auto auto auto',
				}}
			>
				{
					datacurrent?.map(item => {
						return (
							<Card key={item.id}
								sx= {{
									margin: '0px 12px 30px 12px',
									backgroundColor: theme.palette.primary.dark,
									padding: '30px 15px',
									borderRadius: '23px',
								}}
							>
								<CardMedia 
									component= "img"
									image= {item.imageUrl}
									alt= {item.title}
									sx= {{
										borderRadius: '23px',
										cursor: 'pointer',
										width: '100%',
										objectFit: 'cover',
										height: '22.831050228310502vh'
									}}
								/>
								<Box
									sx={{
										display: 'flex',
										justifyContent: 'space-between',
										alignItems: 'center',
										marginTop: '20px',
									}}
								>
									<GameNameContent>{item.title}</GameNameContent>
									<Box
										sx={{
											display: 'flex',
											alignItems: 'center',
										}}
									>
										<StarIcon 
											sx={{
												color: theme.palette.primary.yellow,
												width: '16px',
												height: '16px',
											}}
										/>
										<GameAmountContent>{item.rate}</GameAmountContent>
									</Box>
								</Box>
								<Box
									sx={{
										display: 'flex',
										justifyContent: 'space-between',
										alignItems: 'center',
										marginTop: '7px',
									}}
								>
									<GameTypeContent>{item.subTitle}</GameTypeContent>
									<Box
										sx={{
											display: 'flex',
											alignItems: 'center',
										}}
									>
										<DownloadIcon 
											sx={{
												color: theme.palette.primary.lighter,
												width: '16px',
												height: '16px',
											}}
										/>
										<GameAmountContent>{item.download}</GameAmountContent>
									</Box>
								</Box>
							</Card>
						)
					})
				}
			</Box>
			<Box textAlign="center" mb="-50px" sx={{ visibility: isClicked ? 'hidden' : 'unset'}}>
				<ButtonMore contentButton='Discover Popular' handleOnClick={handleMore}/>
			</Box>
		</Box>
	)
}
