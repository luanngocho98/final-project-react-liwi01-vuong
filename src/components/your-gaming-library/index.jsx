import { Box, Button, Grid, useTheme } from '@mui/material'
import React, {  } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { getYourGaming } from '../../services/your-gaming'
import ButtonMore from '../button'
import Title from '../title'

export default function YourGamingLibrary() {
	const theme = useTheme()
	const { library } = useSelector(state => state.yourgaming)
	const dispathch = useDispatch()
	const TitleContent = styled('div')({
		color: theme.palette.primary.light,
		fontSize: '15px',
    marginBottom: '5px',
		fontWeight: '700',
	})
	const GameContent = styled('div')({
		color: theme.palette.primary.main,
		fontSize: '14px',
	})

	useEffect(() => {
		getYourGaming(
			(res) => {
				const { data } = res
				dispathch({
					type: 'SET_YOURGAMING',
					payload: {
						data
					}
				})
			},
			(err) => {
				console.log(err)
			}
		)
	}, [dispathch])
	return (
		<Box
			sx={{
				padding: '30px',
				backgroundColor: theme.palette.primary.darker,
				borderRadius: '23px',
			}}
		>
			<Box
				sx={{
					marginBottom: '30px',
				}}
			>
				<Title
					titleLeft= 'Your Gaming'
					titleRight= 'Library'
				/>
			</Box>
			<Grid container flexDirection="column">
				{
					library?.map((item) => {
						return (
							<Grid item container key={item.id}
								sx={{
									display: 'flex',
									borderBottom: `1px solid ${theme.palette.primary.dark}`,
									marginBottom: '20px',
									paddingBottom: '20px',
									justifyContent: 'space-between',
									alignItems: 'center',
								}}
							>
								<Grid
									item
									sm={2}
									
								>
									<Box
										sx={{
											'& img': {
												borderRadius: '23px',
												width: '100%',
												height: '100%',
												cursor: 'pointer'
											},
											width: '12.1765601217656vh',
											height: '5.856515373352855vw'
										}}
									>
										<img src={item.imageUrl} alt={item.gameTitle} />
									</Box>
								</Grid>
								<Grid
									item
									sm={2}
								>
									<TitleContent>{item.gameTitle}</TitleContent>
									<GameContent>{item.device}</GameContent>
								</Grid>
								<Grid
									item
									sm={2}
								>
									<TitleContent>Date Added</TitleContent>
									<GameContent>{item.dateAdded}</GameContent>
								</Grid>
								<Grid
									item
									sm={2}
								>
									<TitleContent>Hours Played</TitleContent>
									<GameContent>{item.hoursPlayed}</GameContent>
								</Grid>
								<Grid
									item
									sm={2}
								>
									<TitleContent>Currently</TitleContent>
									<GameContent>{item.downloaded ? 'Downloaded' : 'Hadn’t Downloaded'}</GameContent>
								</Grid>
								<Grid
									item
									sm={2}
								>
									<Button
										sx={{
											borderRadius: '25px',
											border: item.downloaded ? `1px solid ${theme.palette.primary.main}` : `1px solid ${theme.palette.primary.lighter}`,
											padding: '12px 30px',
											textTransform: 'unset',
											color: item.downloaded ? theme.palette.primary.main : theme.palette.primary.lighter,
											':hover': {
												backgroundColor: item.downloaded ? 'transparent' : theme.palette.primary.light,
											}
										}}
									>
										{
											item.downloaded ? 'Downloaded' : 'Download'
										}
									</Button>
								</Grid>
							</Grid>
						)
					})
				}
			</Grid>
			<Box textAlign="center" mb="-50px">
				<ButtonMore contentButton= 'View Your Library'/>
			</Box>
		</Box>
	)
}
