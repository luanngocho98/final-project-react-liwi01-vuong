import { Box, Typography, useTheme } from '@mui/material'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import bannerbg from '../../assets/images/banner-bg.jpg'
import { path } from '../../routers/path'
import ButtonMore from '../button'


export default function BannerHome() {
  const theme = useTheme()
  const navigate = useNavigate()

  const handleOnClickBrowse = () => {
    navigate(path.browse)
  }

  return (
    <Box
        sx={{
          backgroundImage: `url(${bannerbg})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          padding: '12.1765601217656vh 4.392386530014641vw',
          borderRadius: '23px',
          color: theme.palette.primary.light,
          fontSize: '40px',
          fontWeight: '700',
          marginBottom: '60px',
        }}
      >
        <Typography variant='h6'
          sx={{
            marginBottom: '10px',
            fontSize: '20px',
            fontWeight: '400'
          }}
        >
          Welcome To Cyborg
        </Typography>
        <Typography
          onClick={handleOnClickBrowse}
          variant='h4'
          sx={{
            display: 'inline',
            color: theme.palette.primary.lighter,
            fontSize: '45px',
            fontWeight: '700',
            marginRight: '12px',
            cursor: 'pointer',
            ':hover': {
              textDecoration: 'underline'
            }
          }}
        >
          BROWSE
        </Typography>
        <Typography 
          variant='h4'
          sx={{
            display: 'inline',
            fontSize: '45px',
            fontWeight: '700',
          }}
        >
          OUR <br />
          POPULAR GAMES HERE<br />
        </Typography>
        <Box mt={3}>
          <ButtonMore contentButton= 'Browse Now'/>
        </Box>
    </Box>
  )
}
