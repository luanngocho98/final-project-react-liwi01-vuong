export const palette = {
  primary: {
    lighter: '#e75e8d',
    light: '#ffffff',
    main: '#666',
    dark: '#27292a',
    darker: '#1f2122',
    contrastText: '#F99',
    yellow: '#ffff00',
    lighterminus: '#E56D97'
  }
}
