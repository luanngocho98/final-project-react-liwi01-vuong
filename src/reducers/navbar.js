import { path } from "../routers/path";

const initalState = {
  navHeader: [
    {
      id: 1,
      navName: 'Home',
      isActive: true,
      path: path.home,
    },
    {
      id: 2,
      navName: 'Browse',
      isActive: false,
      path: path.browse,
    },
    {
      id: 3,
      navName: 'Details',
      isActive: false,
      path: path.details,
    },
    {
      id: 4,
      navName: 'Streams',
      isActive: false,
      path: path.streams,
    },
    {
      id: 5,
      navName: 'Login',
      isActive: false,
      path: path.login,
    },
  ],
}
const navReducer = (state = initalState, action) => {
  switch (action.type) {
    case 'UPDATE_LOCATION': {
      const copyNavHeader = [...state.navHeader];
      for(let i = 0; i < copyNavHeader.length; i++) {
        if(copyNavHeader[i].path === action.payload.pathname) {
          copyNavHeader[i].isActive = true;
        }else {
          copyNavHeader[i].isActive = false;
        }
      }
      return {
        ...state,
        navHeader: copyNavHeader,
      }
    }
    default:
      return state;
  }
}
export default navReducer;