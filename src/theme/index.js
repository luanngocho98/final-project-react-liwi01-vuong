import { createTheme } from "@mui/material";
import { palette } from "./override/palette";
import { typography } from "./override/typography";

export const theme = createTheme({
  palette: palette,
  typography: typography,
})
