const initalState = {
  library: [],
}

const yourgamingReducer = (state = initalState, action) => {
  switch (action.type){
    case 'SET_YOURGAMING': {
      return {
        ...state,
        library: action.payload.data,
      }
    }
    default:
      return state
  }
}

export default yourgamingReducer;