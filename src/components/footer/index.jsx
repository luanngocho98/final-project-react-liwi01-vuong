import { Box, useTheme } from '@mui/material'
import React from 'react'
import styled from 'styled-components'

export default function Footer() {
	const theme = useTheme()
	const FooterContent = styled('div')({
		color: theme.palette.primary.light,
		fontSize: '19px',
	})

	return (
		<Box
			sx={{
				paddingTop: '30px',
				position: 'relative',
				bottom: '0'
			}}
		>
			<Box sx={{
				display: 'flex',
				justifyContent: 'center',
				marginBottom: '8px'
			}}>
				<FooterContent>Copyright © 2036</FooterContent>
				<Box
					sx={{
						':hover': {
							color: theme.palette.primary.lighter,
						},
						margin: '0px 5px',
						cursor: 'pointer',
						color: theme.palette.primary.light,
						fontSize: '19px',
					}}
				>
					Cyborg Gaming
				</Box>
				<FooterContent>Company. All rights reserved. </FooterContent>
			</Box>
			<Box sx={{
				display: 'flex',
				justifyContent: 'center',
			}}>
				<FooterContent>Design:</FooterContent>
				<Box
					sx={{
						':hover': {
							color: theme.palette.primary.lighter,
						},
						marginLeft: '5px',
						cursor: 'pointer',
						color: theme.palette.primary.light,
						fontSize: '19px',
					}}
				>
					TemplateMo
				</Box>
			</Box>
		</Box>
	)
}
