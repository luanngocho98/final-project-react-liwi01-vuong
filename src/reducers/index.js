import { combineReducers } from "redux"
import authenticationReducer from "./authentication"
import futuredgameReducer from "./futuredgame"
import mostpopularrightnowReducer from "./mostpopularrightnow"
import navReducer from "./navbar"
import yourgamingReducer from "./yourgaming"

const rootReducer = combineReducers({
  authentication: authenticationReducer,
  mostpopularrightnow: mostpopularrightnowReducer,
  yourgaming: yourgamingReducer,
  futuredgame: futuredgameReducer,
  navbar: navReducer,
})
export default rootReducer