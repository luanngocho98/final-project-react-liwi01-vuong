import { Box, Dialog, useMediaQuery, useTheme } from '@mui/material'
import React from 'react'
import { useState } from 'react'
import { Outlet } from 'react-router-dom'
import Footer from '../components/footer'
import Header from '../components/header'
import LoginPage from '../pages/login'

export default function DefaultLayout() {
  const theme = useTheme()
  const [openLogin, setOpenLogin] = useState(false);
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
  console.log(openLogin)

  const handleClose = () => {
    setOpenLogin(false)
  }
  
  return (
    <Box
      sx={{
        padding: '15px 7.686676427525622vw 30px 7.686676427525622vw'
      }}
    >
      <Header setOpenLogin={setOpenLogin}/>
      <Box
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: '23px',
          padding: '4.392386530014641vw',
        }}
      >
        <Outlet />
      </Box>
      <Footer />
      <Dialog
        fullScreen={fullScreen}
        open={openLogin}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        sx={{
          '& .css-1t1j96h-MuiPaper-root-MuiDialog-paper': {
            borderRadius: '36px',
            backgroundColor: theme.palette.primary.dark
          }
        }}
      >
        <LoginPage handleClose={handleClose}/>
      </Dialog>
    </Box>
  )
}
