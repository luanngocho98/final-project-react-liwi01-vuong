export const path = {
  home: '/',
  browse: '/browse',
  details: '/details',
  streams: '/streams',
  login: '/login',
  register: '/register',
}