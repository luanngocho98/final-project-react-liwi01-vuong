import { Box, Typography, useTheme } from '@mui/material'
import React from 'react'

export default function Title(props) {
  const theme = useTheme()
  const { titleLeft, titleRight } = props

  return (
    <Box
      sx={{
        color: theme.palette.primary.lighter,
        fontSize: '34px',
        fontWeight: '700',
      }}
    >
      <Typography
        sx={{
          display: 'inline',
          textDecoration: 'underline',
          color: theme.palette.primary.light,
          fontSize: '34px',
          fontWeight: '700',
        }}
      >
        {titleLeft}
      </Typography>
      &nbsp;{titleRight}
    </Box>
  )
}
