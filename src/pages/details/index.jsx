import { Box, Grid, Typography, useTheme } from '@mui/material'
import React from 'react'
import bannerLeft from '../../assets/images/feature-left.jpg'
import bannerRight from '../../assets/images/feature-right.jpg'
import detail1 from '../../assets/images/details-01.jpg'
import detail2 from '../../assets/images/details-02.jpg'
import detail3 from '../../assets/images/details-03.jpg'
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import StarIcon from '@mui/icons-material/Star';
import DownloadIcon from '@mui/icons-material/Download';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import StorageIcon from '@mui/icons-material/Storage';
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import Title from '../../components/title'


export default function Details() {
  const { fortnite, library } = useSelector(state => state.mostpopularrightnow)
  const theme = useTheme()
  const DataContent = styled('div')({
    fontSize: '14px',
    color: theme.palette.primary.light,
  })
  const GameNameContent = styled('div')({
    fontSize: '15px',
    color: theme.palette.primary.light,
    fontWeight: '700',
  })
  const GameTypeContent = styled('div')({
    fontSize: '14px',
    color: theme.palette.primary.main,
  })


  return (
    <Box>
      {/* Banner Area */}
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between'
        }}
      >
        <Box
          sx={{
            width: '33.33333333%',
            marginRight: '1.7569546120058566vw',
            '& img': {
              borderRadius: '23px',
              width: '100%'
            }
          }}
        >
          <img src={bannerLeft} alt="FORTNITE" />
        </Box>
        <Box
          sx={{
            width: '66.66666667%',
            position: 'relative',
            '& img': {
              borderRadius: '23px',
              width: '100%'
            }
          }}
        >
          <img src={bannerRight} alt="FORTNITE" />
          <Box
            sx={{
              position: 'absolute',
              top: '42%',
              left: '47%',
              borderRadius: '50%',
              padding: '8px 8px 4px 8px',
              backgroundColor: theme.palette.primary.light
            }}
          >
            <PlayArrowIcon sx={{ color: theme.palette.primary.lighter }} />
          </Box>
        </Box>
      </Box>
      {/* Banner Area */}

      {/* FORTNITE Area */}
      <Typography variant='h2'
        sx={{
          fontSize: '45px',
          textAlign: 'center',
          margin: '60px 0px',
          fontWeight: '700',
          color: theme.palette.primary.light,
        }}
      >
        FORTNITE DETAILS
      </Typography>
      {/* FORTNITE Area */}

      {/* FORTNITE Content Area */}
      <Box
        sx={{
          borderRadius: '23px',
          padding: '30px',
          backgroundColor: theme.palette.primary.darker,
        }}
      >
        <Box
          sx={{
            display: 'flex',
          }}
        >
          <Box
            sx={{
              flex: '1',
              backgroundColor: theme.palette.primary.dark,
              borderRadius: '23px',
              padding: '30px',
              marginRight: '24px',
            }}
          >
            <Box>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <GameNameContent>{fortnite.gameName}</GameNameContent>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    '& svg': {
                      width: '16px',
                      height: '16px',
                      color: theme.palette.primary.yellow,
                    }
                  }}
                >
                  <StarIcon />
                  <DataContent>{fortnite.star}</DataContent>
                </Box>
              </Box>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  marginTop: '7px',
                }}
              >
                <GameTypeContent>{fortnite.gameType}</GameTypeContent>
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    '& svg': {
                      width: '16px',
                      height: '16px',
                      color: theme.palette.primary.lighter,
                    }
                  }}
                >
                  <DownloadIcon />
                  <DataContent>{fortnite.downloads}</DataContent>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box
            sx={{
              flex: '1',
              display: 'flex',
              justifyContent: 'space-around',
              backgroundColor: theme.palette.primary.dark,
              borderRadius: '23px',
              padding: '30px',
            }}
          >
            <Box sx={{ textAlign: 'center' }}>
              <StarIcon sx={{ marginBottom: '12px', color: theme.palette.primary.yellow }} />
              <DataContent>{fortnite.star}</DataContent>
            </Box>
            <Box sx={{ textAlign: 'center' }}>
              <DownloadIcon sx={{ marginBottom: '12px', color: theme.palette.primary.lighter }} />
              <DataContent>{fortnite.downloads}</DataContent>
            </Box>
            <Box sx={{ textAlign: 'center' }}>
              <StorageIcon sx={{ marginBottom: '12px', color: theme.palette.primary.lighter }} />
              <DataContent>{fortnite.storage}</DataContent>
            </Box>
            <Box sx={{ textAlign: 'center' }}>
              <SportsEsportsIcon sx={{ marginBottom: '12px', color: theme.palette.primary.lighter }} />
              <DataContent>{fortnite.action}</DataContent>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            marginTop: '30px',
            display: 'flex',
            '& img': {
              borderRadius: '23px',
              width: '100%'
            },
            '& .MuiBox-root': {
              width: '33.33333333%',
            }
          }}
        >
          <Box sx={{ marginRight: '1.7569546120058566vw' }}>
            <img src={detail1} alt="FORTNITE" />
          </Box>
          <Box sx={{ marginRight: '1.7569546120058566vw' }}>
            <img src={detail2} alt="FORTNITE" />
          </Box>
          <Box>
            <img src={detail3} alt="FORTNITE" />
          </Box>
        </Box>
        <Box
          sx={{
            fontSize: '15px',
            color: theme.palette.primary.main,
            lineHeight: '30px',
            marginTop: '30px',
            '& span': {
              color: theme.palette.primary.light,
              cursor: 'pointer',
              ':hover': {
                color: theme.palette.primary.contrastText,
              }
            }
          }}
        >
          Cyborg Gaming is free HTML CSS website template provided by TemplateMo. This is Bootstrap v5.2.0 layout. You can make a&nbsp;<Typography variant='a'>small contribution via PayPal</Typography>&nbsp;to info [at] templatemo.com and thank you for supporting. If you want to get the PSD source files, please contact us. Lorem ipsum dolor sit consectetur es dispic dipiscingei elit, sed doers eiusmod lisum hored tempor.
        </Box>
        <Box
          sx={{
            fontSize: '14px',
            color: theme.palette.primary.lighter,
            backgroundColor: 'transparent',
            border: `1px solid ${theme.palette.primary.lighter}`,
            padding: '12px 30px',
            borderRadius: '23px',
            textAlign: 'center',
            fontWeight: '500',
            cursor: 'pointer',
            marginTop: '30px',
            ':hover': {
              backgroundColor: theme.palette.primary.light,
              borderColor: theme.palette.primary.light,
            }
          }}
        >
          Download Fortnite Now!
        </Box>
      </Box>
      {/* FORTNITE Content Area */}

      {/* Other Related Area */}
      <Box
        sx={{
          borderRadius: '23px',
          padding: '30px',
          backgroundColor: theme.palette.primary.darker,
          marginTop: '60px',
        }}
      >
        <Title
          titleLeft='Other Related'
          titleRight='Games'
        />

        <Grid container spacing={2} sx={{ marginTop: '10px' }}>
          {
            library.map((item) => {
              return (
                <Grid item xs={6}>
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      borderBottom: `1px solid ${theme.palette.primary.dark}`,
                      paddingBottom: '30px',
                      
                      '& img': {
                        borderRadius: '23px',
                        marginRight: '15px',
                        width: '100%'
                      }
                    }}
                  >
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <img src={item.image} alt={item.gameName} />
                      <Box>
                        <Box sx={{ marginBottom: '10px' }}>
                          <GameNameContent>{item.gameName}</GameNameContent>
                        </Box>
                        <Box>
                          <GameTypeContent>{item.gameType}</GameTypeContent>
                        </Box>
                      </Box>
                    </Box>
                    <Box>
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          marginBottom: '9px',
                          '& svg': {
                            width: '16px',
                            height: '16px',
                            color: theme.palette.primary.yellow,
                          }
                        }}
                      >
                        <StarIcon />
                        <DataContent>{item.star}</DataContent>
                      </Box>
                      <Box
                        sx={{
                          display: 'flex',
                          alignItems: 'center',
                          '& svg': {
                            width: '16px',
                            height: '16px',
                            color: theme.palette.primary.lighter,
                          }
                        }}
                      >
                        <DownloadIcon />
                        <DataContent>{item.downloads}</DataContent>
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              )
            })
          }
        </Grid>
      </Box>
      {/* Other Related Area */}
    </Box>
  )
}
