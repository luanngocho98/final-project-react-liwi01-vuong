import { Box, Button, InputAdornment, TextField, useTheme } from '@mui/material'
import React from 'react'
import CloseIcon from '@mui/icons-material/Close';
import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import VisibilityIcon from '@mui/icons-material/Visibility';
import iconGG from '../../assets/logo/IconGoogle.svg'
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
import styled from 'styled-components';

export default function LoginPage(props) {
  const { handleClose } = props
  const theme = useTheme()
  const SecondContent = styled('span')({
    color: theme.palette.primary.lighter,
    fontSize: '16px',
    fontWeight: '700',
    cursor: 'pointer'
  })

  return (
    <Box
      sx={{
        overflowY: 'scroll',
        width: '520px',
        '::-webkit-scrollbar': {
          width: '1px'
        }
      }}
    >
      <Box
        sx={{
          textAlign: 'end',
          '& svg': {
            color: theme.palette.primary.light,
            width: '36px',
            height: '36px',
            padding: '28px 28px 0px 0px',
            cursor: 'pointer'
          }
        }}
      >
        <CloseIcon onClick={handleClose} />
      </Box>
      <Box
        sx={{
          textAlign: 'center',
        }}
      >
        <SportsEsportsIcon sx={{ color: theme.palette.primary.lighter, width: '48px', height: '48px' }}/>
      </Box>
      <Box
        sx={{
          fontSize: '24px',
          marginTop: '18px',
          textAlign: 'center',
          fontWeight: '500',
          lineHeight: '28px',
          color: theme.palette.primary.light
        }}
      >
        Chào mừng bạn đến với CYBORG
      </Box>
      <Box
        sx={{
          padding: '20px 65px 0px',
        }}
      >
        <Box>
          <TextField
            placeholder='Email'
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon
                    sx={{ color: theme.palette.primary.light, zIndex: 1, }}
                  />
                </InputAdornment>
              ),
            }}
            sx={{
              '& .MuiInputBase-root': {
                borderRadius: '23px',
                border: `solid 1px ${theme.palette.primary.main}`,
                backgroundColor: theme.palette.primary.darker,
                fontSize: '15px',
                height: '46px',
              },
              ' & .MuiInputBase-input': {
                color: theme.palette.primary.light,
              },
              width: '100%',
              marginBottom: '20px',
            }}
          />
        </Box>
        <Box>
          <TextField
            placeholder='Mật khẩu'
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon
                    sx={{ color: theme.palette.primary.light, zIndex: 1, }}
                  />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <VisibilityIcon
                    sx={{ color: theme.palette.primary.light, zIndex: 1, cursor: 'pointer' }}
                  />
                </InputAdornment>
              ),
            }}
            sx={{
              '& .MuiInputBase-root': {
                borderRadius: '23px',
                border: `solid 1px ${theme.palette.primary.main}`,
                backgroundColor: theme.palette.primary.darker,
                fontSize: '15px',
                height: '46px',
              },
              ' & .MuiInputBase-input': {
                color: theme.palette.primary.light,
              },
              width: '100%',
              marginBottom: '20px',
            }}
          />
        </Box>
        <Box
          sx={{
            cursor: 'pointer',
            display: 'flex',
            fontSize: '14px',
            marginTop: '8px',
            fontWeight: '700',
            justifyContent: 'end',
            marginBottom: '20px',
            color: theme.palette.primary.lighter,
          }}
        >
          Quên mật khẩu ?
        </Box>
        <Button
          sx={{
            width: '100%',
            textTransform: 'capitalize',
            fontSize: '14px',
            color: theme.palette.primary.light,
            backgroundColor: theme.palette.primary.lighterminus,
            border: 'none',
            padding: '8px 30px',
            borderRadius: '23px',
            textAlign: 'center',
            fontWeight: '700',
            cursor: 'pointer',
            marginBottom: '20px',
            ':hover': {
              backgroundColor: theme.palette.primary.lighter,
            }
          }}
        >
          Đăng nhập
        </Button>
        <Button
          sx={{
            width: '100%',
            textTransform: 'capitalize',
            fontSize: '14px',
            color: theme.palette.primary.light,
            backgroundColor: 'transparent',
            border: `solid 1px ${theme.palette.primary.light}`,
            padding: '8px 30px',
            borderRadius: '23px',
            textAlign: 'center',
            fontWeight: '700',
            cursor: 'pointer',
            marginBottom: '20px',
            display: 'flex',
            alignItems: 'center',
            ' & img': {
              marginRight: '8px',
            }
          }}
        >
          <img src={iconGG} alt="google" />
          Tiếp tục với Gmail
        </Button>
        <Box
          sx={{
            color: theme.palette.primary.light,
            fontSize: '16px',
            margin: '18px 0px 18px 0px',
            textAlign: 'center',
            lineHeight: '20px',
          }}
        >
          Bằng cách tiếp tục, bạn đồng ý với các<br />
          <SecondContent>Điều khoản dịch vụ</SecondContent>&nbsp;của chúng tôi
        </Box>
        <Box
          sx={{
            color: theme.palette.primary.light,
            fontSize: '16px',
            margin: '18px 0px 18px 0px',
            textAlign: 'center',
            lineHeight: '20px',
          }}
        >
          Bạn chưa tham gia Cyborg?&nbsp;
          <SecondContent>Đăng ký</SecondContent>
        </Box>
        <Box
          sx={{
            color: theme.palette.primary.light,
            fontSize: '16px',
            margin: '18px 0px 18px 0px',
            textAlign: 'center',
            lineHeight: '20px',
          }}
        >
          Bạn là doanh nghiệp? Hãy bắt đầu&nbsp;
          <SecondContent>tại đây!</SecondContent>
        </Box>
      </Box>

    </Box>
  )
}
